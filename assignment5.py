#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, 
		 {'title': 'Algorithm Design', 'id':'2'}, 
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return jsonify(r)
   

@app.route('/')

@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
     print("print")

@app.route('/book/<int:book_id>/<string:book_name>/edit/', methods=['GET','POST'])
def editBook(book_id, book_name):
    index = 0
    targetTitle = ""
    targetDict = {}

    while(index < len(books) and targetTitle != book_name):
        targetDict = books[index]
        targetTitle = targetDict['title']
        index+=1
   
    return render_template('editBook.html', book_id = book_id, book_name = targetTitle)

@app.route('/book/<int:book_id>/<string:book_name>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id, book_name):
    index = 0
    stringDelete = ""
    targetDict = {}
    while(index < len(books) and stringDelete != book_name):
        targetDict = books[index]
        stringDelete = targetDict['title']
        index+=1

    return render_template('deleteBook.html', book_id = book_id, book_name = stringDelete)


@app.route('/book/<int:book_id>/<string:book_name>/modifyDict', methods=['GET','POST'])
def modifyDict(book_id, book_name):
    if request.method == 'POST':
        search_key = request.form['name'] # the new name to change to
        index = 0
        targetDict = {}
    
        while(index < len(books)):
            targetDict = books[index]
            if(targetDict['title'] == book_name):
                books[index]['title'] = search_key

            index+=1

        return redirect(url_for('showBook', nm = search_key))
    else:
	    # if my server didn't receive a POST request, it's going to 
	    # redirect the user back to the main index page.
	    # The helper function 'redirect' is used to accomplish that.
	    return render_template('showBook.html')	


@app.route('/book/<int:book_id>/<string:book_name>/deleteEntry', methods=['GET','POST'])
def deleteEntry(book_id, book_name):
   # loop thru and delte the entry with the name

    index = 0
    targetDict = {}
    
    while(index < len(books)):
        targetDict = books[index]
        if(targetDict['title'] == book_name):
           del books[index]

        index+=1

    return render_template('showBook.html', books = books)

"""
@app.route('/book/<int:book_id>/<string:book_name>/addBook', methods=['GET','POST'])
def addBook(book_id, book_name):
   # loop thru and delte the entry with the name
    addDict = {'Title': book_name, 'Title': book_name }
    books.append()
    return render_template('showBook.html', books = books)
"""

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

